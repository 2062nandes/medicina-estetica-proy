(function($){
  $(function(){

    $('.button-collapse').sideNav();
    $('.scrollspy').scrollSpy();
  }); // end of document ready
    $(document).ready(function(){
      $('.collapsible').collapsible();
    });
  $(document).ready(function(){
  	$('#ir-arriba').hide();
  	$(function(){
  		$(window).scroll(function(){
  			if ($(this).scrollTop() > 290) {
  					$('#ir-arriba').fadeIn();
  			}else{
  					$('#ir-arriba').fadeOut();
  			}
  		})
  	});
  	$("#ir-arriba span").click(function(){
  			$("body,html").animate({
  				scrollTop: 0
  			}, 900);
  			return false;
  	})
  });
  $(document).ready(function(){
      $('.slider').slider({full_width: true});
  });
  $(document).ready(function(){
    /*dropdown desplegable*/
      $('.dropdown-button').dropdown({
          hover: true
        });
  });
  /*Efecto paralax*/
  $(document).ready(function(){
    $('.parallax').parallax();
  });

  /*Formulario de contacto*/
  $(function(){
    $('input, textarea').each(function() {
      $(this).on('focus', function() {
        $(this).parent('.input').addClass('active');
     });
    if($(this).val() != '') $(this).parent('.input').addClass('active');
    });
  });

  var message = $('#statusMessage');
  $('.deletebtn').click(function() {
    message.removeClass("animMessage");
    $("#theForm input").css("box-shadow", "none");
    $("#theForm textarea").css("box-shadow", "none");
    $("#theForm select").css("box-shadow", "none");
    $(".input").removeClass("active");
  });
  $('.submitbtn').click(function() {
    message.removeClass("animMessage");
    $("#theForm input").css("box-shadow", "");
    $("#theForm textarea").css("box-shadow", "");
    $("#theForm select").css("box-shadow", "");
    if($("#theForm")[0].checkValidity()){
      $.post("mail.php", $("#theForm").serialize(), function(response) {
        if (response == "enviado"){
          $("#theForm")[0].reset();
          $(".input").removeClass("active");
          message.html("Su mensaje fue envíado correctamente,<br>le responderemos en breve");
        }
        else{
          message.html("Su mensaje no pudo ser envíado");
        }
        message.addClass("animMessage");
        $("#theForm input").css("box-shadow", "none");
        $("#theForm textarea").css("box-shadow", "none");
        $("#theForm select").css("box-shadow", "none");
      });
      return false;
    }
  });
})(jQuery); // end of jQuery name space
