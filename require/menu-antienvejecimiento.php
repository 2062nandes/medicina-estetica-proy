<header>
<nav class="teal darken-1 nav-extended" role="navigation" style="height: 130px;">
  <div class="nav-wrapper container row">

    <div class="col s6">
      <!-- <a id="logo-container"  href="/" class="brand-logo">
        <div style="font-size: 115px; font-weight: 600;position: absolute;z-index: 1;top: 12px;left: -20px;cursor: default;">{ </div>
        <h1 class="medicina">Medicina <br> <b>Estética</b></h1>
      </a> -->
      <a class="logo-titulo" href="./">
        <h1 class="brand-logo dra">Dra. SANDRA IRUSTA</h1>
        <h2 class="brand-logo dra">MEDICINA ESTÉTICA<br>ANTIENVEJECIMIENTO</h2>
      </a>
    </div>
    <ul class="right hide-on-med-and-down">
      <li><a class="botones waves-effect waves-light btn" href="/">Inicio</a></li>
      <li><a class="botones waves-effect waves-light btn" href="curriculum.php">Currículum</a></li>
      <li><a class="botones waves-effect waves-light btn" href="contacto.php">Contacto</a></li>
    </ul>

    <ul id="nav-mobile" class="side-nav">
      <h2 style="color: rgb(38, 166, 154);font-weight: 600;text-align: center; font-size: 25px;">Medicina Estética</h2>
      <h1 style="color: rgb(38, 166, 154);text-align: center; font-size: 21px;font-weight: 500;">Dra. SANDRA IRUSTA</h1>
      <li class="divider"></li>
      <li><a class="waves-effect waves-light btn" href="/">Inicio</a></li>
      <li><a class="waves-effect waves-light btn" href="curriculum.php">Curriculum</a></li>
      <li><a class="waves-effect waves-light btn" href="contacto.php">Contacto</a></li>
      <li class="divider"></li>
      <li><a class="waves-effect waves-light btn" href="medicina-estetica.php">Medicina Estética</a></li>
      <li><a class="waves-effect waves-light btn" href="antienvejecimiento.php">Antienvejecimiento</a></li>
    </ul>
    <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
  </div>
</nav>
<ul id="dropdown1" class="dropdown-content">
  <div class="col s12 m6 l6">
    <li><a class="center-align" href="medicina-estetica.php#tratamiento-de-arrugas-faciales">Tratamientos con botox</a></li>
        <li class="divider"></li>
    <li><a class="center-align" href="medicina-estetica.php#rellenos">Rellenos faciales</a></li>
        <li class="divider"></li>
    <li><a class="center-align" href="medicina-estetica.php#microdermoabrasion">Microdermoabrasión</a></li>
        <li class="divider"></li>
    <li><a class="center-align" href="medicina-estetica.php#radiofrecuencia-y-alta-frecuencia">Radiofrecuencia y Alta frecuencia</a></li>
        <li class="divider"></li>
    <li><a class="center-align" href="medicina-estetica.php#inesteticismos-corporales">Inesteticismo corporales</a></li>
  </div>
  <div class="col s12 m6 l6">
    <li><a class="center-align" href="medicina-estetica.php#cicatrices">Cicatrices</a></li>
        <li class="divider"></li>
    <li><a class="center-align" href="medicina-estetica.php#peeling-quimico">Peeling químico</a></li>
        <li class="divider"></li>
    <li><a class="center-align" href="medicina-estetica.php#depilacion-prolongada">Depilación prolongada</a></li>
        <li class="divider"></li>
    <li><a class="center-align" href="medicina-estetica.php#borrado-de-tatuajes">Borrado de tatuajes</a></li>
  </div>
</ul>
<ul id="dropdown2" class="dropdown-content">
  <li><a class="center-align" href="#luz-pulsada-intensa">Luz pulsada intensa</a></li>
  <li class="divider"></li>
  <li><a class="center-align" href="#laser-nd-yag">Laser Nd YAG.</a></li>
  <li class="divider"></li>
  <li><a class="center-align" href="#intradermoterapia-facial-capilar-y-corporal">Intradermoterapia facial, capilar y corporal</a> </li>
  <li class="divider"></li>
  <li><a class="center-align" href="#plasma-rico-en-plaquetas">Plasma rico en plaquetas</a></li>
</ul>
<nav class="nav-extended z-depth-4 teal darken-1">
  <div class="nav-wrapper container">
    <div class="row">
        <a class="col s12 m6 l6 dropdown-button waves-effect waves-light btn-large" style="border: solid 2px #00897b;" data-beloworigin="true" data-activates="dropdown1" href="medicina-estetica.php">
          <h2>MEDICINA ESTÉTICA</h2><i class="material-icons right">arrow_drop_down</i>
        </a>
        <a class="col s12 m6 l6 dropdown-button waves-effect waves-light btn-large" style="border: solid 2px #00897b;" data-beloworigin="true" data-activates="dropdown2" href="antienvejecimiento.php">
          <h2>ANTIENVEJECIMIENTO</h2><i class="material-icons right">arrow_drop_down</i>
        </a>
    </div>
  </div>
</nav>
</header>
