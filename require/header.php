<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>Medicina Estética — Dra. Sandra Irusta</title>
  <!-- <link rel="icon" href="favicon.png" type="image/png"> -->
  <meta name="designer" content="Fernando Javier Averanga Aruquipa"/>
  <meta name="description" content="Medicina estética, antienvejecimiento, Tratamiento de arrugas faciales, rellenos, plasma rico en plaquetas, adiposidades localizadas, cicatrices, luz pulsada intensa IPL, laser Nd Yag, radiofrecuencias,  aclaramiento facial y corporal, depilación prolongada, borrado de tatuajes." />
  <meta name="author" content="Dra. Sandra Irusta Maceda" />
  <meta name="keywords" content="Medicina estética en Bolivia" />
  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Raleway|Source+Sans+Pro" rel="stylesheet">
  <link href="css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
