<footer class="page-footer teal darken-1">
  <div class="container">
    <div class="row" style="margin-bottom:10px;">
      <div class="col s12 m12 l4">
        <div class="fwrap">
          <h5 class="white-text center-align">NUESTROS SERVICIOS</h5>
          <ul id="datos" style="text-align:center;">
            <div class="col s12 m12 l12" style="padding-right: 0;">
              <li style=""><a href="/">- Inicio</a></li>
              <li style=""><a href="curriculum.php">- Curriculum</a></li>
              <li style=""><a href="contacto.php">- Contacto</a></li>
              <li style="background:rgba(0, 163, 129, 0.61); height: 1px;padding:1px;margin:5px"></li>
              <li><a href="medicina-estetica.php">>> MEDICINA ESTÉTICA</a></li>
              <li><a href="antienvejecimiento.php">>> ANTIENVEJECIMIENTO</a></li>
            </div>
          </ul>
        </div>
      </div>
      <div class="col l5 m12 s12">
        <h5 class="white-text center-align">DIRECCIÓN LA PAZ — BOLIVIA</h5>
        <div class="fwrap">
            <div id="datos">
              <p class="grey-text text-lighten-4 center-align">Z. Sopocachi, Av. 6 de Agosto # 2300 esq. Rosendo Gutierrez Ed. Jardiel Mzz Of. 2 <a href="https://www.google.com/maps/d/viewer?mid=1_GITJdJhztH3XMOi01vWDsR1XXQ&ll=-16.508325807586594%2C-68.12678935000002&z=17" target="_blank"><i class="material-icons">room</i> Ver mapa</a></p>
              <p class="grey-text text-lighten-4 center-align"><i style="top:10px" class="material-icons">settings_phone</i> Telf.: 2415272 <span class="opt"> &nbsp;&nbsp;</span> <i class="material-icons">settings_cell</i>Cel.: 72015723</p>
              <p class="white-text center-align"><i class="material-icons">email</i> sandra_irusta@yahoo.es <span class="opt"> &nbsp;&nbsp;</span><img src="images/whatsapp.png" width="22" alt="Whatsapp - Dra Sandra Irusta"> Whatsapp: 72015723</p>
            </div>
        </div>
      </div>
      <div class="col l3 m12 s12 center-align">
          <h1 class="white-text" style="font-size:25px;margin-top: 12px;">MEDICINA ESTÉTICA</h1>
          <div class="fwrap">
              <div id="datos">
              <h2 class="white-text" style="font-size:19px; margin:0;">Dra. Sandra Irusta Maceda</h2><br>
              <p class="white-text">Visitenos en: </p>
              <div class="botsocial">
                  <p>
                      <a href="https://www.facebook.com/DraSandraIrusta/" target="_blank"><img src="images/facebook.png" alt="próximamente en Facebook"></a>
                      <a href="#" target="_blank"><img src="images/google.png" alt="próximamente en Google +"></a>
                      <a href="#" onclick="void();" target="_blank"><img src="images/twitter.png" alt="próximamente en Youtube"></a>
                  </p>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
  <div class="teal darken-2">
    <div class="container">
      <div class="row" style="margin-bottom: 0px;">
          <div class="col s12 l8 white-text center-align" style="font-size: 14px;margin-top: 1.5%;">
              Todos los Derechos Reservados &REG; <strong style="font-weight:800;">Medicina Estética — Dra. Sandra Irusta.</strong> &COPY; <?=date("Y");?>.
          </div>
          <div class="col s12 l4 white-text center-align" style="color: rgba(255, 255, 255, 0.22);">
              <p class="ah"><a href="//ahpublic.com" target="_blank" id="ahpublicidad">Diseño y Programación Ah! Publicidad</a></p>
          </div>
      </div>
    </div>
  </div>
</footer>

<div id="ir-arriba">
  <a href="javascript:void(0);"><span class="btn-floating btn-large waves-effect z-depth-4"><i class="material-icons">keyboard_capslock</i></span></a>
</div>
<!--  Scripts-->
<script src="js/jquery.js"></script>
<script src="js/materialize.min.js"></script>
<script src="js/init.js"></script>
