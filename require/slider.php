<div class="slider teal lighten-1">
  <ul class="slides">
    <li>
      <img src="http://www.absolutebeauty.es/wp-content/uploads/fondo.jpg"> <!-- random image -->
      <div class="caption center-align">
        <h3>This is our big Tagline!</h3>
        <h5 class="light grey-text text-lighten-3">Here's our small slogan.</h5>
      </div>
    </li>
    <li>
      <img src="http://www.absolutebeauty.es/wp-content/uploads/fondo.jpg"> <!-- random image -->
      <div class="caption left-align">
        <h3>Left Aligned Caption</h3>
        <h5 class="light grey-text text-lighten-3">Here's our small slogan.</h5>
      </div>
    </li>
    <li>
      <img src="http://www.absolutebeauty.es/wp-content/uploads/fondo.jpg"> <!-- random image -->
      <div class="caption right-align">
        <h3>Right Aligned Caption</h3>
        <h5 class="light grey-text text-lighten-3">Here's our small slogan.</h5>
      </div>
    </li>
    <li>
      <img src="http://www.absolutebeauty.es/wp-content/uploads/fondo.jpg"> <!-- random image -->
      <div class="caption center-align">
        <h3>This is our big Tagline!</h3>
        <h5 class="light grey-text text-lighten-3">Here's our small slogan.</h5>
      </div>
    </li>
  </ul>
</div>
