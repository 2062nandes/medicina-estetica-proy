                  <div class="col s12">
                    <i class="bien material-icons col s1">done</i><p class="col s11 p">Médico Cirujano . Universidad Nuestra Señora de La Paz – 2002</p>
                  </div>
                  <div class="col s12">
                    <i class="bien material-icons col s1">done</i><p class="col s11 p">Especialidad Clínica Estética, AMA 2004</p>
                  </div>

                  <div class="col s12">
                    <i class="bien material-icons col s1">done</i><p class="col s11 p">Medicina Estética ,Universidad Jhon F. Kennedy, Buenos Aires Argentina</p>
                  </div>
                  <div class="col s12">
                    <i class="bien material-icons col s1">done</i><p class="col s11 p">Medicina Estética , otorgado por Unión Internacional Europea - UIME</p>
                  </div>
                  <div class="col s12">
                    <i class="bien material-icons col s1">done</i><p class="col s11 p">Medicina Anti- Aging, otorgado por Unión Internacional Europea - UIME</p>
                  </div>
                  <div class="col s12">
                    <i class="bien material-icons col s1">done</i><p class="col s11 p">Medicina Antienvejecimiento Y Longevidad, Universidad Autónoma de Tlaxcala México</p>
                  </div>

                  <div class="col s12">
                    <i class="bien material-icons col s1">done</i><p class="col s11 p">Instituto Pinto: Estética facial</p>
                  </div>
                  <div class="col s12">
                    <i class="bien material-icons col s1">done</i><p class="col s11 p">Miembro de la Sociedad Argentina de Medicina Estética - SOARME</p>
                  </div>
                  <div class="col s12">
                    <i class="bien material-icons col s1">done</i><p class="col s11 p">Miembro fundador de la Asociación de Médicos Estéticos La Paz Bolivia</p>
                  </div>
                  <div class="col s12">
                    <i class="bien material-icons col s1">done</i><p class="col s11 p">Hospital Militar Argerich: Actualización en láser y luz pulsada intensa</p>
                  </div>
                  <div class="col s12">
                    <i class="bien material-icons col s1">done</i><p class="col s11 p">Soarme , Curso de bioseguridad para uso de láser.</p>
                  </div>
                  <div class="col s12">
                    <i class="bien material-icons col s1">done</i><p class="col s11 p">Excelencia profesional en salud – IOCIM, MÉXICO 2015.</p>
                  </div>
                  <div class="col s12">
                    <i class="bien material-icons col s1">done</i><p class="col s11 p">PRICE TO THE MEDICAL BY ACHIEVEMENT FOR A BETTER LIFE , México 2015</p>
                  </div>
                  <div class="col s12">
                    <i class="bien material-icons col s1">done</i><p class="col s11 p">Miembro activo de la organización internacional para la capacitacion e investigacion medica .</p>
                  </div>
                  <div class="col s12">
                    <i class="bien material-icons col s1">done</i><p class="col s11 p">Ponente en congreso internacional de salud en MÉXICO , Tema fotoenvejecimiento y propuestas clínicas terapéuticas.</p>
                  </div>
                  <div class="col s12">
                    <i class="bien material-icons col s1">done</i><p class="col s11 p">PREMIO LAQI , Certificado a la Calidad Boliviana, Bolivia 2016</p>
                  </div>
                  <div class="col s12">
                    <i class="bien material-icons col s1">done</i><p class="col s11 p">QUALITY AWARDS, Bolivia 2017</p>
                  </div>
                  <div class="col s12">
                    <i class="bien material-icons col s1">done</i><p class="col s11 p">HEALT CARE PRIZE IOCIM , Veracruz, México 2017</p>
                  </div>