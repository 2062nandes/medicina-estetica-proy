<!DOCTYPE html>
<html lang="es">
<?php require('require/header.php') ?>
<body class="teal lighten-4">
  <?php require('require/menu.php'); ?>

  <div class="section no-pad-bot" style="padding-top: 0;" id="index-banner">
    <div class="container">
      <?php //require('require/slider.php'); ?>
      <div class="parallax-container z-depth-1">
            <div class="parallax"><img src="images/paralax.jpg"></div>
            <div class="text-parallax">
                <h3><span>TRATAMIENTOS CON BOTOX</span></h3>
                <h3><span>RELLENOS FACIALES</span></h3>
                <h3><span>RADIOFRECUENCIA</span></h3>
                <h3><span>ALTA FRECUENCIA</span></h3>
                <h3><span>ULTRASONIDO</span></h3>
                <h3><span>LUZ PULSADA INTENSA (IPL)</span></h3>
                <h3><span>LASER Nd YAG.</span></h3>
                <h3><span>INTRADERMOTERAPIA FACIAL,</span></h3>
                <h3><span>CAPILAR, CORPORAL</span></h3>
                <h3><span>PEELING QUÍMICO ACLARANTE</span></h3>
                <h3><span>Y ANTI AGE</span></h3>
                <h3><span>DEPILACIÓN PROLONGADA</span></h3>
                <h3><span>BORRADO DE TATUAJES</span></h3>
                <h3><span>PLASMA RICO EN PLAQUETAS</span></h3>
            </div>
      </div>
      <!-- <h1 class="header center orange-text">Starter Template</h1>
      <div class="row center">
        <h5 class="header col s12 light">A modern responsive front-end framework based on Material Design</h5>
      </div>
      <div class="row center">
        <a href="http://materializecss.com/getting-started.html" id="download-button" class="btn-large waves-effect waves-light orange">Get Started</a>
      </div> -->
    </div>
  </div>


  <!-- <div class="container">
    <div class="section"> -->

      <!--   Icon Section   -->
      <!-- <div class="row">
        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center light-blue-text"><i class="material-icons">flash_on</i></h2>
            <h5 class="center">Speeds up development</h5>

            <p class="light">We did most of the heavy lifting for you to provide a default stylings that incorporate our custom components. Additionally, we refined animations and transitions to provide a smoother experience for developers.</p>
          </div>
        </div>

        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center light-blue-text"><i class="material-icons">group</i></h2>
            <h5 class="center">User Experience Focused</h5>

            <p class="light">By utilizing elements and principles of Material Design, we were able to create a framework that incorporates components and animations that provide more feedback to users. Additionally, a single underlying responsive system across all platforms allow for a more unified user experience.</p>
          </div>
        </div>

        <div class="col s12 m4">
          <div class="icon-block">
            <h2 class="center light-blue-text"><i class="material-icons">settings</i></h2>
            <h5 class="center">Easy to work with</h5>

            <p class="light">We have provided detailed documentation as well as specific code examples to help new users get started. We are also always open to feedback and can answer any questions a user may have about Materialize.</p>
          </div>
        </div>
      </div> -->
    <!-- </div>
    <br><br> -->
  </div>
 <?php require('require/footer.php'); ?>
  </body>
</html>
