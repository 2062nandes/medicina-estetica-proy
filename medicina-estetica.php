<!DOCTYPE html>
<html lang="es">
<?php require('require/header.php') ?>
<body>
  <?php require('require/menu-medicina-estetica.php'); ?>
  <div class="container">
    <h2 class="titulo">Medicina Estética</h2>
    <section class="section no-pad-bot">
        <div class="row">
          <div class="col s12 m12 l12">
                <h3 id="tratamiento-de-arrugas-faciales" class="titulo section scrollspy">Tratamientos con botox</h3>
                <div class="img-revista">
                  <!-- <div class="col s6 m3 l3">
                    <img class="responsive-img" src="images\arrugas-faciales-antes-2.jpg">
                        <p class="center-align">ANTES</p>
                      </div>
                      <div class="col s6 m3 l3">
                        <img class="responsive-img" src="images\arrugas-faciales-despues-2.jpg">
                        <p class="center-align">DESPUÉS</p>
                      </div> -->
                      <div class="col s6 m3 l3">
                        <img class="responsive-img" src="images\arrugas-faciales-antes.jpg">
                        <p class="center-align">ANTES</p>
                      </div>
                      <div class="col s6 m3 l3">
                        <img class="responsive-img" src="images\arrugas-faciales-despues.jpg">
                        <p class="center-align">DESPUÉS</p>
                      </div>
                      <div class="col s12 m6 l6">
                        <div class="video-responsive">
                          <iframe src="https://www.youtube.com/embed/8KVIJZvT6Z8?rel=0" frameborder="0" allowfullscreen></iframe>
                        </div>
                      </div>  
                    </div>
                    <p class="justificado revista">
                      .<br>
                      La toxina botulínica es la mejor opción para rejuvenecer el tercio
                      superior del rostro, que incluye  arrugas de la frente, entrecejo y
                      arrugas periorbiculares ( Patas de gallo ).</p>
                      <p class="justificado revista">Dará aspecto natural de la piel, paraliza el músculo en cuestión
                        responsable de la arrugas dinámicas o de expresión , suavizará  y
                        minimizará las arrugas de reposo.</p>
                        <!-- <p class="justificado revista">La aplicación se realiza 1 sesión después de 6 o incluso 8 meses</p> -->
                        <!-- <p class="justificado revista">Contraindicado en pacientes embarazadas o con alergias en la piel.</p> -->
                        <!-- <p class="justificado revista">En manos de un profesional médico especializado carece de efectos secundarios</p> -->
                      </div>
                      <div class="col s12 m12 l12">
                        </div>
              <div class="col s12 m12 l12">
                <div>
                  <div style="float: right;" class=" img-revista col s12 m8 l6">
                </div>
                  <p class="justificado revista"></p>
                  <p class="justificado revista"></p>
                  <p class="justificado revista"></p>
                  <p class="justificado revista"></p>
                  <p class="justificado revista"></p>
                  <p class="justificado revista"></p>
                  <p class="justificado revista"></p>
                </div>
            </div>
        <div class="col s12 m12 l12">
            <h3 id="rellenos" class="titulo section scrollspy">Rellenos faciales</h3>
          <div>
            <div style="float: right;" class=" img-revista col s12 m8 l6">
              <div class="col s6">
                <img class="responsive-img" src="images\rellenos-faciales-antes-2.jpg">
                <p class="center-align">ANTES</p>
              </div>
              <div class="col s6">
                <img class="responsive-img" src="images\rellenos-faciales-despues-2.jpg">
                <p class="center-align">DESPUÉS</p>
              </div>
            </div>
            <p class="justificado revista">El ácido hialurónico, aporta volumen y extraordinaria hidratación mejora la calidad y apariencia de la piel.</p>
            <p class="justificado revista">El ácido hialurónico es el único producto aprobado para el uso de rellenos en el rostro, está presente en todos los tejidos humanos por eso es tan seguro , no necesita test de alergia.</p>
            <p class="justificado revista">Rejuvenece tercio inferior del rostro, atenuando los surcos nasogenianos y surcos mentonianos (líneas de marioneta). Aporta volumen a los labios.</p>
            <p class="justificado revista">Las sesiones se realizan cada 8 a 12 meses (1 sesión al año).</p>
            <p class="justificado revista">Los rellenos están contraindicados  en embarazo,periodo de  lactancia, piel con heridas o infecciones y piel con alergia.</p>
          </div>
        </div>
        <div class="col s12 m12 l12">
          <div>
            <div style="float: right;" class=" img-revista col s12 m8 l6">
          </div>
            <p class="justificado revista"></p>
            <p class="justificado revista"></p>
            <p class="justificado revista"></p>
            <p class="justificado revista"></p>
          </div>
      </div>
      <div class="col s12 m12 l12">
          <h3 id="microdermoabrasion" class="titulo section scrollspy">Microdermoabrasión</h3>
            <div class="col s12 m8 l6">
              <div class="img-revista">
                <div class="col s6">
                  <img class="responsive-img" src="images\microdermoabrasion-antes.jpg">
                  <p class="center-align">ANTES</p>
                </div>
                <div class="col s6">
                  <img class="responsive-img" src="images\microdermoabrasion-despues.jpg">
                  <p class="center-align">DESPUÉS</p>
                </div>
              </div>
            </div>
              <!-- <p class="justificado revista">La microdermoabrasión constituye uno de los últimos tratamientos empleados estética.</p> -->
              <p class="justificado revista">Es un procedimiento clínico con el cual se consigue eliminar o exfoliar las capas exteriores de células muertas de la piel , promoviendo la regeneración celular, incrementando la producción de colágeno y aumentando la elasticidad.</p>
              <p class="justificado revista">Con la microdermoabrasión se elimina y mejora cicatrices, se disminuye las líneas de expresión y arrugas, después de la sesión de microdermoabrasión la piel se siente más limpia, más brillante y más suave , uniforme y posteriormente ayuda a la piel a que se renueve naturalmente.</p>
              <!-- <p class="justificado revista">La microdermoabrasión presenta las puntas de diamante que producen la remoción celular y tiene un mecanismo  de sucesión que , según el nivel que se ejerza, determinará el grado de expoliación en la piel, así, siendo fuerte la succión , la exfoliación será más profunda.</p>
              <p class="justificado revista">Inestetismo tratados con la microdermoabrasión. -->
              <br>
                <div class="col s12 m6 l3">
                  <li class="punto-li">Acne</li>
                  <li class="punto-li">Cicatrices de acné</li>
                  <li class="punto-li">Líneas de expresión</li>
                </div>
                <div class="col s12 m6 l3">
                  <li class="punto-li">Arrugas</li>
                  <li class="punto-li">Fotodaño</li>
                  <li class="punto-li">Piel seca</li>
                </div>
                <div class="col s12 m6 l3">
                  <li class="punto-li">Tatuajes</li>
                  <li class="punto-li">Manchas</li>
                  <li class="punto-li">Maquillaje permanente</li>
                </div>
                <div class="col s12 m6 l3">
                  <li class="punto-li">Secuelas de varicela</li>
                  <li class="punto-li">Cicatrices protuberantes</li>
                  <li class="punto-li">Cicatrices deprimidas</li>
                  <li class="punto-li">Queloides</li>
                </div>
              </p>
              <p class="justificado revista">Los cambios son inmediatos en las cicatrices de acné sus puntos negros (comedones) son ablandados y más fáciles de eliminar , las cicatrices se hacen menos perceptibles las líneas de expresión son menos evidentes , las manchas se atenúan ,etc.<br>
              El resultado final dependerá del número de sesiones de microdermoabrasión que realice el paciente , también dependerá de las condiciones de la piel y el tiempo que haya transcurrido desde que el paciente presente alguna de estas entidades.</p>
              <p class="justificado revista">Las sesiones se realizan una vez por semana y el tiempo depende del área y tratar todas las sesiones se realizan en consultorio de manera ambulatoria , no superando los 15 minutos por sesión.</p>
              <p class="justificado revista"><strong>Efectos Adversos</strong> <br>
                Este procedimiento realizado por un médico especialista que que conoce la técnica no presenta algún efecto adverso al contrario se tiene como resultado una mejor apariencia y una aceptable condición estética, lo que está totalmente comprometido con la autoestima .
                <br>Recordemos “ verse bien para sentirse bien “
            </p>
      </div>
      <div class="col s12 m12 l12">
        <h3 id="radiofrecuencia-y-alta-frecuencia" class="titulo section scrollspy">Radiofrecuencia y Alta frecuencia</h3>
        <div>
          <div style="float: right;" class=" img-revista col s12 m8 l6">
          <div class="col s6">
            <img class="responsive-img" src="images\radiofrecuencia-antes.jpg">
                  <p class="center-align">ANTES</p>
          </div>
          <div class="col s6">
            <img class="responsive-img" src="images\radiofrecuencia-despues.jpg">
                  <p class="center-align">DESPUÉS</p>
          </div>
        </div>
          <p class="justificado revista">La radiofrecuencia produce un calentamiento profundo que afecta a la piel y tejido graso subcutáneo. Un calentamiento que podríamos decir va de dentro hacia fuera. Dicho Calentamiento Va a favorecer:</p>
          <p class="justificado revista">Posee importantes efectos desinfectantes, descongestivos, antiinflamatorios y estimulantes de los tejidos.</p>
          <li class="justificado revista">El drenaje linfático, lo cuál permitirá disminuir los líquidos y las toxinas en el que se encuentran embebidos los adipositos del tejido afecto de celulitis.</li>
          <li class="justificado revista">Un aumento en la circulación de la zona que permitirá mejorar el metabolismo tanto del tejido graso subcutáneo como la mejora del aspecto de la piel acompañante.</li>
          <li class="justificado revista">La formación de nuevo colágeno, tanto en la piel como en el tejido subcutáneo, permitiendo que todo el tejido adquiera firmeza gracias a la reorganización de los septos fibrosos y engrosamiento dérmico suprayacente.</li>
          <li class="justificado revista">Y por último tras la lesión térmica controlada con retracción del tejido hay una respuesta inflamatoria que se verá acompañada de migración de fibroblastos, lo cual reforzará aún más la estructura de colágeno, dando como resultado un rejuvenecimiento de la zona tratada.</li> <br>
        </div>
      </div>
      <div class="col s12 m12 l12">
          <h3 id="inesteticismos-corporales" class="titulo section scrollspy">Inesteticismo corporales</h3>
          <h4 class="titulo section scrollspy">Adiposidades localizadas</h4>
              <div class="col s12 m8 l6">
              <div class="img-revista">
                <div class="col s6">
                  <img class="responsive-img" src="images\inesteticismo-corporales-antes.jpg">
                  <p class="center-align">ANTES</p>
                </div>
                <div class="col s6">
                  <img class="responsive-img" src="images\inesteticismo-corporales-despues.jpg">
                  <p class="center-align">DESPUÉS</p>
                </div>
              </div>
              </div>
              <p class="justificado revista">Sería extraño pensar que alguien siempre está conforme con sus características personales, como ser: color del pelo, ojos, estatura, forma corporal y hasta carácter, personalidad y ocupaciones.
              <p class="justificado revista">Es por eso que frente a este panorama altamente subjetivo, controvertido subconsciente y diverso es muy frecuente la consulta al especialista en medicina estética para corregir mejorar y hasta cambiar su contorno corporal.</p>
              <p class="justificado revista">El grupo de pacientes que realiza su consulta tiene clarísima su disconformidad.</p>
              <p class="justificado revista">Inesteticismo corporaless más frecuentes:</p>
                <li class="punto-li">Obesidad</li>
                <li class="punto-li">Sobrepeso (mínimo, medio severo)</li>
                <li class="punto-li">Tejido celular subcutáneo (desordenado esclerosado, nodular)</li>
                <li class="punto-li">Adiposidades puntuales localizadas (tejido adiposo excesivo en regiones       específicas)</li>
                <li class="punto-li">Flacidez (por tejido celular sin sostén o músculos hipotróficos o atróficos.</li>
                <li class="punto-li">Estrías (por ruptura o lesión de las fibras elásticas, que constituyen la matriz de sostén)</li>
                <li class="punto-li">Edemas (agudos o crónicos)</li>
                <li class="punto-li">Lesiones flebologicas (insuficiencias, varices, telangiectasias y otros)</li>
             <p class="justificado revista">Causas de inesteticismos corporales:
                <div class="col s12 m6 l6">
                  <li class="punto-li">Congénitas,</li>
                  <li class="punto-li">Adquiridas,</li>
                  <li class="punto-li">Hereditarias,</li>
                  <li class="punto-li">Enfermedades de base (hipotiroidismo),</li>
                  <li class="punto-li">Consumo de fármacos ( corticotеrapia, reemplazо hormonal, suplementos y complementos vitaminicos, antidepresivos y  ansiolíticos),</li>
                </div>
                <div class="col s12 m6 l6">
                  <li class="punto-li">Secuelas de enfermedades,</li>
                  <li class="punto-li">Complicaciones e imprevistos (post embarazo, menarca, post lactancia),</li>
                  <li class="punto-li">Latrogenia,</li>
                  <li class="punto-li">Calidad de vida,</li>
                  <li class="punto-li">Hábitos tabaquismo, alcoholismo, etc.</li>
                  <li class="punto-li">Psicopatías, etc.</li>
<br>
                </div>
           <p class="justificado revista">La desinformación o hiperinformación tal vez se convirtió en un boomerang cuando la mayoría de las personas decidió empezar a ocuparse de sus formas corporales; algunos alimentándose poco y otros intentando todo tipo de esfuerzos, saltos, pesas, bailes, golpes e impactos, perdiendo electrolitos necesarios para vivir, agotando sus músculos; otros en cambio consumiendo energizantes anabólicos, y lo docenas de huevos para lograr el cuerpo deseado.</p>
           <p class="justificado revista"><strong>Soluciones</strong>
           <br>El el arsenal terapéutico de clínica estética contamos con aparatología, tratamientos locales y sistémicos para cada entidad antiestética que preocupe al paciente, los cuales son ambulatorios.</p>
      </div>
      <div class="col s12 m12 l12">
          <h3 id="cicatrices" class="titulo section scrollspy">Cicatrices y secuelas de acné</h3>
        <div>
          <div style="float: right;" class=" img-revista col s12 m8 l6">
          <div class="col s6">
            <img class="responsive-img" src="images\cicatrices-antes.jpg">
                  <p class="center-align">ANTES</p>
          </div>
          <div class="col s6">
            <img class="responsive-img" src="images\cicatrices-despues.jpg">
                  <p class="center-align">ANTES</p>
          </div>
        </div>
          <p class="justificado revista">Las secuelas o cicatrices del acné son el resultado de los comedones que se infectaron e inflamaron y nunca sanaron correctamente.</p>
          <p class="justificado revista">Estas cicatrices son visibles porque se encuentra un nivel más bajo con respecto a la superficie de la piel.</p>
          <p class="justificado revista">Las cicatrices y la capacidad de la piel para sanar se varían en cada persona lo que depende de los genes de cada uno, siendo algunos resistentes y otro con piel mucho más sensible.</p>
          <p class="justificado revista">El acné constituye la enfermedad de la piel más común en el mundo, presentándose con mayor frecuencia en los adolescentes y adultos jóvenes, afectando a ambos sexos y con un curso más prolongado en el sexo femenino. El acné no es sólo enfermedad sino también un problema estético, con repercusión psicológica que afecta la vida social de las personas que lo padecen comprometido totalmente con la autoestima.</p>
          <p class="justificado revista">Las secuelas o cicatrices del acné son de dos tipos pigmentadas y los agujeros o pocios en la piel.</p>
          <p class="justificado revista">Las cicatrices pigmentadas son manchas que varían de un color rosa a café que quedan cuando los comedones (espinillas o puntos negros) desaparecen.</p>
          <p class="justificado revista">Esta pigmentación es el resultado de la concentración de melanina, cuando la piel es más oscura a mayor tendencia a mancharse.</p>
          <p class="justificado revista">Los pocitos o agujeros en la piel son más comunes en la personas con piel clara, este es el resultado de la formación de pústulas en la piel (comedones inflamados conteniendo pus).</p>
          <p class="justificado revista">El proceso de cicatrización es la forma con la que el cuerpo sana y reemplaza la piel perdida o dañada.</p>
          <p class="justificado revista"><strong>Tratamiento</strong> <br>
            El las cicatrices de acné lo que se debe lograr es la regeneración de la piel. Las cicatrices pigmentadas deben ser aclaradas o desmanchadas. Al se el acné una condición crónica que sólo podría darse durante la adolecencia, puede dejar cicatrices toda la vida.</p>
        </div>
      </div>
      <div class="col s12 m12 l12">
          <h3 id="peeling-quimico" class="titulo section scrollspy">Peeling químico</h3>
          <h4 class="titulo section scrollspy">Aclaramiento facial y corporal</h4>
              <div class="col s12 m8 l6">
              <div class="img-revista">
                <div class="col s6">
                  <img class="responsive-img" src="images\peeling-quimico-antes.jpg">
                  <p class="center-align">ANTES</p>
                </div>
                <div class="col s6">
                  <img class="responsive-img" src="images\peeling-quimico-despues.jpg">
                  <p class="center-align">DESPUÉS</p>
                </div>
              </div>
              </div>
              <p class="justificado revista">
                El peeling constituye uno de los tratamientos estéticos más populares , que ha evolucionado de manera espectacular en los últimos años.
                <br>Desde la antigüedad se fueron realizando procedimientos similares .
                <br>Los egipcios se frotaban la piel con polvo de alabastro los trucos chamuscaban su piel con fuego.
                <br>Los indios aplicaban orina sobre su piel definimos como peeling a la acción de pelar, desmarcar, exfoliar y estimular a través de un acto profesional , realizado con un elemento lo menos tóxico posible para el paciente , que provoque una respuesta epidérmica o dermoepidérmica positiva, con efectos mediatos e inmediatos , dejando como resultado una apariencia aceptable desde el punto de vista social.
              </p>
              <p class="justificado revista">Los peeling se clasifican como superficial, medio y profundo. <br>
                <li class="punto-li">EL peeling superficial es aquel que actúa sobre la capa córnea o exfoliando la capa granulosa . Sus indicaciones ; poros dilatados , falta de brillo, manchas superficiales, aspecto desvitalizado. Como resultado tenemos una piel limpia , suave, brillante, más clara</li>
                <li class="punto-li">El peeling medio actúa sobre toda la epidermis , está indicado en arrugas finas , manchas profundas, cicatrices de acné y pieles fotoenvejecidas. Como resultado estimula la producción de colágeno , estimula la producción de elastina y fibrilina, mejora la elasticidad y turgencia de la piel , disminuyendo las arrugas finas y secuelas en la piel .</li>
                <li class="punto-li">El peeling profundo está indicado en arrugas muy profundas , en fotoenvejecimiento , flacidez y cicatrices profundas ; como resultado se obtiene una renovación parcial o total de las capas dermoepidérmicas, reepitelización completa, acción tensora, piel rosada y efecto lifting.</li>
              </p>
              <p class="justificado revista">Este tratamiento se lo realiza de manera ambulatoria, en consultorio en un tiempo promedio que no supera los 15 minutos. Los cambios se hacen evidentes después de una semana de postratamiento</p>
              <!-- <p class="justificado revista">El peeling químico es un procedimiento no invasivo en donde vamos a eliminar capas superficiales de células muertas. Con este procedimiento vamos a tratar las secuelas de acné, las manchas solares, y las pequeñas arrugas que con el paso del tiempo han aparecido en nuestra piel, Vamos a conseguir mitigar las ojeras y dar brillo y luminosidad a nuestro rostro.</p>
              <p class="justificado revista"><strong>¿Qué es? ¿Para qué estó indicado?</strong>
              <br>El peeling químico es una técnica no invasiva que se realiza para tratar diversas alteraciones de la piel. Este tratamiento está indicado para tratar daños provocados por la exposición solar, acné, pequeñas arrugas y pigmentación irregular.</p> -->
      </div>
      <div class="col s12 m12 l12">
        <h3 id="depilacion-prolongada" class="titulo section scrollspy">Depilación prolongada</h3>
        <div>
          <div style="float: right;" class=" img-revista col s12 m8 l6">
          <div class="col s6">
            <img class="responsive-img" src="images\depilacion-prolongada-antes.jpg">
            <p class="center-align">ANTES</p>
          </div>
          <div class="col s6">
            <img class="responsive-img" src="images\depilacion-prolongada-despues.jpg">
            <p class="center-align">DESPUÉS</p>
          </div>
        </div>
          <!-- <p class="justificado revista">La Depilación prolongada tiene por objetivo obtener la desaparición completa ó parcial del vello en una zona tratada. Hasta la fecha sólo hay dos procedimientos que han demostrado ser efectivos. La Depilación Eléctrica y la Depilación Láser.</p> -->
          <p class="justificado revista">La luz pulsada intensa, sistema abreviado como IPL, es un tipo de tecnología que utiliza luz policromática (es decir, de varios colores) para eliminar los folículos pilosos, o lo que es lo mismo, la raíz de los pelos.</p>
          <p class="justificado revista">Este tipo de depilación luz pulsada no es tan conocida como la depilación láser, aunque estos últimos años ha ganado bastante popularidad debido a que trabaja con mayor eficacia en pieles más oscuras y es indolora.</p>
          <p class="justificado revista"></p>
          <p class="justificado revista"></p>
          <p class="justificado revista"></p>
          <p class="justificado revista"></p>
        </div>
      </div>
      <div class="col s12 m12 l12">
        <h3 id="borrado-de-tatuajes" class="titulo section scrollspy">Borrado de tatuajes</h3>
          <div class="col s12 m8 l6">
              <div class="img-revista">
                <div class="col s6">
                  <img class="responsive-img" src="images\borrado-tatuaje-antes.jpg">
                    <p class="center-align">ANTES</p>
                </div>
                <div class="col s6">
                  <img class="responsive-img" src="images\borrado-tatuaje-despues.jpg">
                  <p class="center-align">DESPUÉS</p>
                </div>
              </div>
          </div>
              <p class="justificado revista">Los tatuajes permanecen toda la vida, sólo se eliminan con la técnica Láser Nd YAG con maquinaria tipo Q-Switched, éste facilita la eliminación de los tatuajes incluso de colores dificultosos sin dañar la piel, es rápido y seguro.</p>
              <p class="justificado revista">El rayo láser emite energía que se dirige hacia el tatuaje en una fracción de segundo, atravesando las capas superficiales de la piel, el pigmento se destruye porque causa una ruptura de las partículas en porciones mucho más pequeñas que son más tarde eliminadas por el sistema linfático (devorados por los macrofagos).</p>
      </div>
      </div><!-- fin de row -->
      <li class="divider"></li>
      <p class="col s12 center-align"><i class="material-icons">play_arrow</i> Todos los tratamientos requieren una consulta previa con el médico estético.</p>
      <p class="col s12 center-align"><i class="material-icons">play_arrow</i> El resutado final del tratamiento puede variar en cada paciente.</p>
  </section>
  </div>
 <?php require('require/footer.php'); ?>

  <script type="text/javascript">

  </script>
  </body>
</html>
