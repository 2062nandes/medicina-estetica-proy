<!DOCTYPE html>
<html lang="es">
<?php require('require/header.php') ?>
<body>
  <?php require('require/menu-antienvejecimiento.php'); ?>
  <div class="container">
    <h2 class="titulo">Antienvejecimiento</h2>
    <section class="section no-pad-bot">
      <div class="row">
        <div class="col s12 m12 l12">
            <h3 id="luz-pulsada-intensa" class="titulo section scrollspy">Luz pulsada intensa</h3>
                <div class="col s12 m8 l6">
                <div class="img-revista">  
                  <div class="col s12 m6 l6">
                    <img class="responsive-img" src="images\luz-pulsada.jpg">
                  </div>
                  <div class="col s12 m6 l6">
                    <img class="responsive-img" src="images\luz-pulsada-2.jpg">
                  </div>
                </div>
                </div>
                  <p class="justificado revista">La luz Pulsada intensa ( IPL ) , Inventado por el Doctor Patrick Bitters. Es un moderno sistema de rejuvenecimiento facial , es versátil  , es decir con un solo equipo se pueden realizar varios tratamientos , mediante la selección de las diferentes longitudes de onda , que se obtienen con la colocación de filtros.</p>
                  <p class="justificado revista">El procedimiento se realiza con un haz intenso de luz de amplio aspectro , dirigido hacia la dermis , diferente al al laser que opera con luz muy especifica.</p>
                  <p class="justificado revista">Su principio, es calentar y estimular la Dermis , al mismo tiempo que se enfría y protege la Epidermis.</p>
                  <p class="justificado revista">El efecto de rejuvenecimiento se da porque el IPL calienta la dermis , asi estimula la produccion de colageno  porque tiene acción sobre los fibroblastos, mejorando la textura y disminuyendo las arrugas.</p>
                  <p class="justificado revista">Aclara las manchas y elimina capilares dilatados, el IPL es absorbido por la hemoglobina y la melamina , que resulta en daño a la pared del vaso y fragmentación del pigmento que es la melamina , Los pequeños vasos y el pigmento de melanina son reabsorbidos por el cuerpo y se hacen menos visibles.</p>
                  <p class="justificado revista">Las sesiones se realizan cada cuatro semanas el numero de sesiones varia entre 3 a 6, mientrasmas foto daño exista, mayor sesiones necesitara el paciente.</p>
                  <p class="justificado revista">Está contraindicado realizar tratamientos con IPL, en fototipos V y VI de la escala de fitzpatrick, pieles recientemente bronceado , pacientes con medicación fotosensible, pacientes embarazadas.</p>
                  <p class="justificado revista">El procedimiento debe ser realizado por un profesional médico especializado , para seguridad del paciente.</p>
        </div>
        <div class="col s12 m12 l12">
          <h3 id="laser-nd-yag" class="titulo section scrollspy">Laser Nd YAG</h3>
          <div>
            <div style="float: right;" class=" img-revista col s12 m8 l4">
            <div class="col s12 m12 l12">
              <img class="responsive-img" src="images\laser-nd-yag.jpg">
            </div>
            <!-- <div class="col s6">
              <img class="responsive-img" src="http://www.sochiof.cl/images/contenido-no-disponible.jpg">
            </div> -->
          </div>
            <p class="justificado revista">El láser de Nd YAG es excelente para el tratamiento de las lesiones vasculares superficiales, tatuajes y lesiones pigmentadas benignas. Este láser Nd YAG, con una longitud de onda de 1.064 nm, es muy efectivo para tratar los tatuajes de tinta negra-azul y con 532 nm se pueden eliminar las lesiones vasculares y pigmentadas superficiales benignas.</p>
            <p class="justificado revista"></p>
            <p class="justificado revista"></p>
            <p class="justificado revista"></p>
            <p class="justificado revista"></p>
            <p class="justificado revista"></p>
            <p class="justificado revista"></p>
          </div>
      </div>
      <div class="col s12 m12 l12">
          <h3 id="intradermoterapia-facial-capilar-y-corporal" class="titulo section scrollspy">Intradermoterapia facial, capilar y corporal</h3>
              <div class="col s12 m12 l7">
              <div class="img-revista">
                <div class="col s12 m6 l6">
                  <img class="responsive-img" src="images\intradermoterapia-1.jpg">
                </div>
                <div class="col s12 m6 l6">
                  <img class="responsive-img" src="images\intradermoterapia-2.jpg">
                </div>
              </div>
              </div>
              <p class="justificado revista">Consiste en la aplicación de medicamentos directamente en el área a tratar. Cuando tomamos un medicamento por vía oral, el recorre un largo camino, pasa por el sistema digestivo, es filtrado por el hígado y circula a través del sistema vascular llegando a todos los órganos. Después de todo este recorrido sólo una pequeña parte del medicamento llega al lugar que será tratado.</p>
              <p class="justificado revista">La intradermoterapia se utiliza para combatir la celulitis, grasa localizada, estrías y piel flácida.</p>
              <p class="justificado revista">La Intradermoterapia provoca una mayor circulación de sangre en la región del cuero cabelludo, mayor oxigenación y nutrición de los folículos pilosos. Serán necesarias algunas sesiones para que haya resultado. El número de sesiones y el intervalo entre ellas dependerán de cada caso y será determinado por su médico.</p>
              <p class="justificado revista">Además pueden ser necesarias algunas sesiones de acompañamiento para prevenir que la caída del cabello evolucione.</p>
              <p class="justificado revista"></p>
      </div>
      <div class="col s12 m12 l12">
        <h3 id="plasma-rico-en-plaquetas" class="titulo section scrollspy">Plasma rico en plaquetas</h3>
        <div>
          <div class="col s12 m6 l6">
            <div class="video-responsive">
              <iframe src="https://www.youtube.com/embed/XlTGifxQjIY?controls=0&rel=0" frameborder="0" allowfullscreen></iframe>
            </div>
          </div>
          <div style="float: right;" class=" img-revista col s12 m6 l6">
              <div class="col s6">
                  <img class="responsive-img" src="images\plasma-rico-en-plaquetas-antes.jpg">
                  <p class="center-align">ANTES</p>
              </div>
              <div class="col s6">
                  <img class="responsive-img" src="images\plasma-rico-en-plaquetas-despues.jpg">
                  <p class="center-align">DESPUÉS</p>
              </div>
        </div>
          <p class="justificado revista">.<br></p>
          <p class="justificado revista">El plasma rico en plaquetas es un tratamiento autólogo, es decir, se utiliza la misma sangre del paciente y es ambulatorio, cuyo fundamento es utilizar los Factores de Crecimiento que se encuentran dentro de las plaquetas sanguíneas.</p>
          <p class="justificado revista"></p>
          <p class="justificado revista"></p>
          <p class="justificado revista"></p>
          <p class="justificado revista"></p>
          <p class="justificado revista"></p>
          <p class="justificado revista"></p>
        </div>
      </div>
    </div><!-- fin de row -->
    <li class="divider"></li>
    <p class="col s12 center-align"><i class="material-icons">play_arrow</i> Todos los tratamientos requieren una consulta previa con el médico estético.</p>
    <p class="col s12 center-align"><i class="material-icons">play_arrow</i> El resutado final del tratamiento puede variar en cada paciente.</p>
  </section>
  </div>
 <?php require('require/footer.php'); ?>
  </body>
</html>
