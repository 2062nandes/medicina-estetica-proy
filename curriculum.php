<!DOCTYPE html>
<html lang="es">
<?php require('require/header.php') ?>
<body>
  <?php require('require/menu.php'); ?>
  <div class="container">
    <h2 class="titulo">Curriculum</h2>
    <section class="section no-pad-bot">
          <div class="row">
           <div class="drairusta-block">
            <div class="col l7 m12 s12">
            <?php require('require/curriculum-text.php'); ?>
            </div>
            <div class="col l5 offset-m2 m8 s12">
                <img style="width:100%;" src="images\Dra_Sandra_Irusta.jpg?v=1.03">
                <h1 style="font-size:26px; font-weight:bold;margin:0 5px" class="center-align">Dra. Sandra Irusta Maceda</h1>
            </div>
           </div>
<!--RESPONSIVE HTML-->
        <div class="drairusta-responsive">
            <div class="col offset-m2 m8 s12">
                <img style="width:100%;" src="images\Dra_Sandra_Irusta.jpg?v=1.03">
                <h1 style="font-size:26px; font-weight:bold;margin:0 5px" class="center-align">Dra. Sandra Irusta Maceda</h1>
            </div>
            <div class="col s12">
            <?php require('require/curriculum-text.php'); ?>                  
            </div>
        </div>
      </div><!-- fin de row -->
  </section>
  </div>
 <?php require('require/footer.php'); ?>
  </body>
</html>
