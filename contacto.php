<!DOCTYPE html>
<html lang="es">
  <?php require('require/header.php') ?>
<body>
  <?php require('require/menu.php'); ?>
<div class="container">
  <!-- <h2 class="titulo">Contáctenos</h2> -->
  <div class="row contactos" style="margin: 0;">
      <div class="col l3 m4 s12" style="padding: 0 0;">
        <h4>Contáctenos</h4>
            <form style="padding: 21px 4% 20px 1%;" method="post" id="theForm" class="second" action="mail.php" role="form">
                      <div class="form_row">
                        <div class="input input-field">
                          <i class="material-icons prefix">account_circle</i>
                          <input type="text" id="nombre" class="validate" name="nombre" tabindex="1" required>
                          <label for="nombre">Nombre completo:</label>
                        </div>
                      </div>

                      <div class="form_row">
                        <div class="input input-field">
                          <i class="material-icons prefix">phone</i>
                          <input type="text" id="telefono" name="telefono" tabindex="2" required>
                          <label for="telefono">Teléfono:</label>
                        </div>
                      </div>

                      <div class="form_row">
                        <div class="input input-field">
                          <i class="material-icons prefix">settings_cell</i>
                          <input type="text" id="movil" name="movil" tabindex="3" required>
                          <label for="movil">Teléfono móvil:</label>
                        </div>
                      </div>

                      <div class="form_row">
                        <div class="input input-field">
                          <i class="material-icons prefix">location_on</i>
                          <input type="text" id="direccion" name="direccion" tabindex="4" required>
                          <label for="direccion">Dirección:</label>
                        </div>
                      </div>

                      <div class="form_row">
                        <div class="input input-field">
                          <i class="material-icons prefix">location_city</i>
                          <input type="text" id="ciudad" class="validate" name="ciudad" tabindex="5" required>
                          <label for="ciudad">Ciudad:</label>
                        </div>
                      </div>
                        <div class="form_row">
                          <div class="input input-field">
                            <i class="material-icons prefix">email</i>
                            <label for="email">Su E-mail:</label>
                            <input type="email" id="email" name="email" tabindex="6" required>
                          </div>
                        </div>

                        <div class="form_row mensaje">
                          <div class="input input-field">
                            <i class="material-icons prefix">mode_edit</i>
                            <label for="mensaje">Mensaje:</label>
                            <textarea id="mensaje" class="materialize-textarea" cols="55" rows="7" name="mensaje" tabindex="7" required></textarea>
                          </div>
                        </div>

                        <div class="form_row botones center-align">
                          <input class="submitbtn waves-effect waves-yellow btn z-depth-3" type="submit" tabindex="8" value="Enviar"> </input>
                          <!-- <input class="deletebtn waves-effect waves-yellow btn z-depth-3" type="reset" tabindex="9" value="Borrar"> </input> -->
                        </div>
                    <div class="col s12">
                      <div id="statusMessage"></div>
                    </div>
            </form>
        </div>
        <div class="col l9 m8  s12" style="padding: 0 0;">
          <h4>Ubíquenos</h4>
              <iframe src="https://www.google.com/maps/d/u/0/embed?mid=1_GITJdJhztH3XMOi01vWDsR1XXQ" width="640" height="360"></iframe>
        </div><!-- fin de colf2 -->
    </div><!-- fin de row -->
  </div>
 <?php require('require/footer.php'); ?>
  </body>
</html>
